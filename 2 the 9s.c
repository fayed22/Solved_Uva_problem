#include<stdio.h>
#include<string.h>
int main()
{
    int i,l,count,sum,p;
    char in[1050];
    while(scanf("%s",&in))
    {
        if(in[0]=='0')
            break;
        sum=0;
        count=0;
        l=strlen(in);
        for(i=0;i<l;i++)
        {
            sum=sum+in[i]-'0';
        }
        if(sum%9==0)
        {
            count++;
            while(sum!=9)
            {
                p=sum;
                sum=0;
                while(p!=0)
                {
                    sum=sum+(p%10);
                    p=p/10;
                }
                count++;
            }
            printf("%s is a multiple of 9 and has 9-degree %d.\n",in,count);
        }
        else
        {
            printf("%s is not a multiple of 9.\n",in);
        }
    }
    return 0;
}
